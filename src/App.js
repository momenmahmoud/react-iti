import { useState } from 'react';
import slides from './slides';
import Carousel from 'react-bootstrap/Carousel';

import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';

function App() {
  const [index, setIndex] = useState(0);

  const handleSelect = (selectedIndex) => {
    const firstSlideCheck = selectedIndex === 0 && index === slides.length - 1;
    const lastSlideCheck = selectedIndex === slides.length - 1 && index === 0;
    if (firstSlideCheck || lastSlideCheck) return;
    setIndex(selectedIndex);
  };
  return (
    <div className="app">
      <Carousel
        fade={true}
        interval={null}
        activeIndex={index}
        onSelect={handleSelect}
      >
        {slides.map(({ img, title, subtitle, content, code }, index) => {
          const finalContent = content?.split(' ');
          return (
            <Carousel.Item className="item" key={index}>
              {img ? <img src={img} className="image" alt={title} /> : null}
              {title ? <h1>{title}</h1> : null}
              {subtitle ? <h3>{subtitle}</h3> : null}
              {finalContent ? (
                <p>
                  {finalContent.map((word) =>
                    word[0] === '"' && word[word.length - 1] === '"' ? (
                      <b>{word} </b>
                    ) : (
                      `${word} `
                    )
                  )}
                </p>
              ) : null}
              {code ? <code>{code}</code> : null}
            </Carousel.Item>
          );
        })}
      </Carousel>
    </div>
  );
}

export default App;
