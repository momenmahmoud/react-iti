const slides = [
  {
    title: 'React JS',
    subtitle: 'Modern React - Hooks - Context - Router',
  },
  {
    subtitle: 'React History',
    content: `
    React was created by Jordan Walke, a software engineer at Facebook, who released an early prototype of React called "FaxJS".
    He was influenced by XHP, an HTML component library for PHP.
    It was first deployed on Facebook's News Feed in 2011 and later on Instagram in 2012.
    It was open-sourced at JSConf US in May 2013.
    On April 18, 2017, Facebook announced React Fiber, a new core algorithm of React library for building user interfaces.
    React Fiber was to become the foundation of any future improvements and feature development of the React library.
    On September 26, 2017, React 16.0 was released to the public.
    On February 16, 2019, React 16.8 was released to the public. The release introduced React Hooks.
    On August 10, 2020, the React team announced the first release candidate for React v17.0
    notable as the first major release without major changes to the React developer-facing API.`,
  },
  {
    subtitle: `
    React is a JavaScript library
    "React - ReactDOM - ReactScripts"
    `,
  },
  {
    subtitle: 'JSX',
    code: `
      const element = <h1>Hello, world!</h1>;

      const element = React.createElement({
        h1,
        {},
        Hello, world!
      });
    `,
  },
  {
    subtitle: 'Why JSX ?',
    content: `
      Embedding Expressions in JSX
      "You can put any valid JavaScript expression inside the curly braces in JSX."
      
      JSX is an Expression Too
      "After compilation, JSX expressions become regular JavaScript function calls and evaluate to JavaScript objects."
      "This means that you can use JSX inside of if statements and for loops, assign it to variables,
      accept it as arguments, and return it from functions"
      
      JSX Prevents Injection Attacks
      "By default, React DOM escapes any values embedded in JSX before rendering them."

      JSX Represents Objects
      "You can think of them as descriptions of what you want to see on the screen.
      React reads these objects and uses them to construct the DOM and keep it up to date."
    `,
  },
  {
    subtitle: 'React Only Updates What’s Necessary',
    content: `
    React DOM compares the element and its children to the previous one,
    and only applies the DOM updates necessary to bring the DOM to the desired state.`,
    code: `
    function tick() {
      const element = (
        <div>
          <h1>Hello, world!</h1>
          <h2>It is {new Date().toLocaleTimeString()}.</h2>
        </div>
      );
      ReactDOM.render(element, document.getElementById('root'));
    }
    
    setInterval(tick, 1000);
    `,
  },
  {
    subtitle: 'Components and Props',
    content: `
    Components let you split the UI into "independent" and "reusable" pieces, and think about each piece in "isolation" .
    Conceptually, components are like JavaScript functions.
    They accept arbitrary inputs called "props" and return React elements describing what should appear on the screen.`,
  },
  {
    subtitle: 'Handling Events',
    code: `
    <button onclick="console.log('Hi there'); return false;">
      Hi there
    </button>

    const log = (e) => {
      console.log('Hi there');
      e.preventDefault();
    }

    <button onClick={log}>
      Hi there
    </button>
    `,
  },
  {
    subtitle: 'Conditional Rendering',
    content: `Conditional rendering in React works the same way conditions work in JavaScript.
    Use JavaScript operators like if or the conditional operator to create elements representing the current state,
    and let React update the UI to match them.`,
    code: `
    const List = (props) => {
      const { loading, error, list } = props;
      if (loading) return <p>Loading</p>;
      if (error) return <p>{error}</p>;
      return <div>{list}</div>;
    }
    `,
  },
  {
    subtitle: `State and Lifecycle ( useState - useEffect )`,
    content: `Component Lifecycles is "Mount" - "Update" - "Unmount" 
    useEffect : triggers the component lifecycle so you can do some magic when it happens.
    useState : defines new state which cause component update.
    To make component rerender, one of two things should happen "State" changes or "Props" changes
    In applications with many components, it’s very important to free up resources taken by the components when they are destroyed.
    `,
  },
];
export default slides;
